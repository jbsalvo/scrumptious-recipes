from django.contrib import admin
from recipes.models import Recipe, Measurement, FoodItem, Ingredient, Step


admin.site.register(Recipe)
admin.site.register(Measurement)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
admin.site.register(Step)
