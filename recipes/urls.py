from django.urls import path

# update for list view, detail view, create view and update view
# from recipe.views import RecipeListView, RecipeDetailView, RecipeUpdateView, RecipeChangeView


from recipes.views import (
    create_recipe,
    change_recipe,
    show_recipe,
    show_recipes,
)

urlpatterns = [
    path("", show_recipes, name="recipes_list"),
    path("<int:pk>/", show_recipe, name="recipe_detail"),
    path("new/", create_recipe, name="recipe_new"),
    path("edit/", change_recipe, name="recipe_edit"),
    # add paths from views.py for as_view()
    # path("create", RecipeCreateView.as_view(), name= "recipe_create")
    # path("<int:pk>/update/", RecipeUpdateView.as_view(), name= "recipe_edit")
]
